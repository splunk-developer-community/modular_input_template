[get_NAME_project://<name>]
*Configure data collection from GitLab

group_type = <value>
* Specify whether the below ID is associated to a GROUP or PROJECT

project_id = <value>
* Enter a Project ID to collect data for a specific project. N.B. The inputs need to be configured on a per-project basis to guarantee event collection. A single input across all projects is limited.

auth = <value>
* This is the Stanza name for the auth account you wish to use located in ta_auth.conf

key = <value>
* This is the key

secret = <value>
* This is the secret

code = <value>
* This is the code

callback_url = <value>
* This can be obtained via GitLab > Profile Settings > Access Tokens. N.B: The token must have permissions to access both the api and read_user in order to obtain everything it needs.

scope = <value>
* This can be obtained via GitLab > Profile Settings > Access Tokens. N.B: The token must have permissions to access both the api and read_user in order to obtain everything it needs.
