# Introduction
This is intended to be a template for use by the Splunk community to aid in creating lightweight add-ons (TA's) for Splunk. This is targeted towards intermediate / advanced users of Splunk who are familiar with creating TA's.

# Structure
This has been based on the TA for GitLab and you will see references within the template towards GitLab. These have been left in as an easy reference to understand how to implement your own changes.

The template itself consists of a bin directory to house all the python code. Along with the default and metadata directories. It also includes CI/CD configuration to work directly with GitLab pipelines

# Getting Started
This assumes you have already researched your API and have documented your goals. The vast majority of your changes will need to take place within the toolset_main.py file. It's recommended that this is left as-is so as to preserve pre-defined references throughout the built-in modules.

## File Prep
Give your stanza in `inputs.conf` an appropriate name and modify it as required. This rename change will also need to take place with its namesake .py file within `bin` and the stanza in `README/inputs.conf.spec`

## Create Inputs
Changes will need to be applied in `toolset_main.py` to the `get_scheme` function, the `set_inputs` in the `ToolsetFunctions` class and within appropriate areas in the `ToolsetInputs` class. In addition, changes will also be required within `inputs.conf.spec` to ensure they match with the scheme defined in `get_scheme`

## Processing Changes
To ensure reliability, changes are suggested in `toolset_main.py` in the `ToolsetFunctions` class  as appropriate. Any API specifics can be applied in the `ToolsetAPI` class

## Data Collection
All data collection is controlled within the `get_events` function in the `ToolsetData` class. This function is responsible for calling sub-functions which are intended to closely work with data from the API and store this in Splunk. An example of how this is put together is in the template.

# Standards
## Internal Logging
All internal logging, without exception, should be handled by the `internal.auth_logger` function. There are numerous examples with how to use this. The primary reason for this is to maintain a consistent output for the logs, and to enable full use of the TA Monitoring Console

### Specifics
* The start of each function should have a START logging message, as can be seen throughout the template.
* The function name should be stored within the logging dict thats passed into the central function.
* A log should be written after each return from a function whereby internal logging has been setup. This enables the monitoring console to put the correct function linkage together
* The first hard-coded input sent to auth_logger should be unique to the function. Making it easy to identify the source of the log message when debugging
* The `prep_event_write` function should be used for writing out data to splunk. This logs out details of the created event for summarisation and display in the MC

## Auth
`creds.py` has been designed to act as a central reference for interacting with the cred lib. Further info as to how to use this is available within the file itself

## Checkpointing
`kv_collection.py` has been written to interact with the KV store for you. Primarily to handle the time checkpointing so duplicate events are not captured. In order for this to work correctly, accurate error handling between functions needs to be carried out

## Input Masking
`creds.py` is mainly responsible for carrying out this task. Inputs initially are all plain text, however creds will ensure they are replaced in the UI, with the values securely stored in the cred lib.

## OAuth
`oauth.py` has been written to perform the initial request and handle any refresh token logic that may be required

## Shared auth
The example contains a field entitled auth. This will act as the main auth account reference and can be used to share the same credentials between inputs

# Additional Info
CI/CD has been built into this template and is available for use. All variable and file references within the yml file will need to be noted and changed appropriately in order for this to function correctly
