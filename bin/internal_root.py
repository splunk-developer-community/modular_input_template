"""
Internal functions requiring use of sub methods

Version 1.0

Purpose:
Initial file called to handle internal TA functions requiring use of other sub-methods.

Usage Instructions:
Changes to this file should reference supporting functions and classes in other files.
Specific logic should be separated from this file to limit the changes required

"""

import xml.dom.minidom
import xml.sax.saxutils

import westylib.internal as internal
import toolset_main

import splunklib.client as client

class IntegrationFunctions():
    """
    Purpose:
    Storage of most generic internal functions.

    Functions externally referenced:
    - All

    """

    def __init__(self, config_str, exec_logger):

        self.config_str = config_str
        self.exec_logger = exec_logger
        self.toolset = None

        self.logging = {}
        self.logging['file'] = "internal_root"
        self.logging['function_name'] = ""

    def initialize_toolset(self):
        """
        Purpose:
        Initializes toolset functions mapping.

        INPUTS:
        - NONE

        NOTE: Uses self variables for info to handle connection
        """
        self.toolset = toolset_main.ToolsetFunctions()


    def create_session(self):
        """
        Purpose:
        Used to create a connection with Splunk.

        INPUTS:
        - NONE

        NOTE: Uses self variables for info to handle connection
        """

        # Initial Logging
        self.logging['function_name'] = "create_session"

        internal.auth_logger(self.exec_logger, self.logging, "START", 'DEBUG', "Starting Function")

        # parse the config XML
        doc = xml.dom.minidom.parseString(self.config_str)

        root = doc.documentElement

        # extract the session key
        session_key = root.getElementsByTagName("session_key")[0]

        # Prepare connection
        self.initialize_toolset()
        args = {'token':session_key.firstChild.data, "app":self.toolset.get_app_name()}

        internal.auth_logger(self.exec_logger,
                             self.logging,
                             "RETCON",
                             'DEBUG',
                             "Returning With Connection")

        # connect and return
        return client.connect(**args)
