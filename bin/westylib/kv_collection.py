"""
KV Store Handler

Version 1.0

Purpose:
Integration process with KV Store.

"""

import json
import time

from . import test_handler, internal


def kv_content(cred_id, curtime):
    """
    Purpose:
    Returns value to be stored in KV Store.

    INPUTS:
    - cred_id: id for entry
    - curtime: current timestamp

    """

    ret_val = {}
    ret_val['input_name'] = str(cred_id)
    ret_val['test_val'] = "ABC"
    ret_val['lastrun'] = curtime

    return ret_val

class KVHandler():
    """
    Purpose:
    Class to handle KV Store Interaction.

    Functions externally referenced:
    - get_checkpoint: get values from KV Store
    - save_checkpoint: save values to KV Store

    """

    def __init__(self, splunk_service, exec_logger, toolset):
        self.splunk_service = splunk_service
        self.splunk_service.namespace['owner'] = 'Nobody'
        self.exec_logger = exec_logger
        self.toolset = toolset

        self.logging = {}
        self.logging['file'] = "kv_collection"
        self.logging['function_name'] = ""
        self.logging['loop_count'] = None

        self.vars = {}
        self.vars['credential'] = "``westy_cred_sep``"
        self.vars['collection'] = self.toolset.get_collection_name()
        self.vars['timestamp'] = ""

    def get_checkpoint(self, stanza):
        """
        Purpose:
        returns value from KV store.

        INPUTS:
        - stanza: Full input stanza

        """

        # Initialize logging
        self.logging['function_name'] = "get_checkpoint"

        logging_val = {}
        logging_val['stanza'] = stanza
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Set ID
        cred_id = stanza

        logging_val = {}
        logging_val['cred_id'] = cred_id
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GETVAL", 'DEBUG', "Getting Value")
        self.logging['data'] = None

        # Record current time to use later
        self.vars['timestamp'] = time.time()

        # Initialize KV Store
        if self.vars['collection'] in self.splunk_service.kvstore:

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "COLLNAMETRUE", 'DEBUG', "Collection Name exists in KV")

            # Get records from KV Store
            collection = self.splunk_service.kvstore[self.vars['collection']]
            collection_data = collection.data.query()

            logging_val = {}
            logging_val['collection'] = json.dumps(collection.data.query())
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "TEMP", 'DEBUG', "Temp Output")
            self.logging['data'] = None

            # Handle when data in collection record is empty
            if str(collection_data) == "[]":

                # The warning message here is questionable. First runs will expect to fall into this
                internal.auth_logger(
                    self.exec_logger,
                    self.logging,
                    "RETUNKNOWNARRAY", 'WARN', "Returning Unknown. Array Empty")

                return "Unknown"

            # Iterate through returned values
            run_checkpoint = \
                [line for line in collection_data \
                    if 'input_name' in line and line['input_name'] == str(cred_id)]

            if run_checkpoint:

                internal.auth_logger(
                    self.exec_logger,
                    self.logging,
                    "RETLASTRUN", 'DEBUG', "Returning Last Run Val")

                return str(run_checkpoint[0]["lastrun"])

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETUNKNOWNEND", 'WARN', "Returning Unknown Val")

        return "Unknown"

    def save_checkpoint(self, stanza):
        """
        Purpose:
        updates KV Store record.

        INPUTS:
        - stanza: Full input stanza

        """

        # Initialize logging
        self.logging['function_name'] = "save_checkpoint"

        logging_val = {}
        logging_val['stanza'] = stanza
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        cred_id = stanza

        logging_val = {}
        logging_val['cred_id'] = cred_id
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # set current time
        curtime = self.vars['timestamp']

        if self.vars['collection'] in self.splunk_service.kvstore:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "COLLNAMETRUE", 'DEBUG', "Collection Name exists in KV")

            # Get KV Store record
            collection = self.splunk_service.kvstore[self.vars['collection']]
            collection_data = collection.data.query()

            logging_val = {}
            logging_val['collection_data'] = str(collection_data)
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CDOUTPUT", 'DEBUG', "Outputting Collection_Data")
            self.logging['data'] = None

            # Check record already has data.
            run_checkpoint = test_handler.test_empty_array(collection_data)
            if run_checkpoint:
                run_checkpoint = \
                    [x for x in collection_data \
                        if 'input_name' in x and x['input_name'] == str(cred_id)]

            # Confirmed that record can and should be updated
            if run_checkpoint:
                internal.auth_logger(
                    self.exec_logger,
                    self.logging,
                    "COLLNAMEUPDATE", 'DEBUG', "Collection Updating")
                collection_update = collection.data.update(run_checkpoint[0]["_key"], \
                    json.dumps(kv_content(cred_id, curtime)))
            else:
                # Value does not exist in record and needs to be inserted
                logging_val = {}
                logging_val['collection_data'] = str(collection_data)
                logging_val['kv_content'] = json.dumps(kv_content(cred_id, curtime))
                self.logging['data'] = json.dumps(logging_val)
                internal.auth_logger(
                    self.exec_logger,
                    self.logging,
                    "COLLNAMEINSERT", 'DEBUG', "Collection Inserting")
                self.logging['data'] = None
                collection_update = collection.data.insert( \
                    json.dumps(kv_content(cred_id, curtime)))
        else:
            # Record needs to be created
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "COLLNAMEFALSE", 'DEBUG', "Collection Name missing in KV: Creating")

            self.splunk_service.kvstore.create(self.vars['collection'])
            collection_update = collection.data.insert( \
                json.dumps(kv_content(cred_id, curtime)))

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "COLLNAMERETURN", 'DEBUG', "Collection Name Returned")
        return collection_update
