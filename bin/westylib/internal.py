"""
Internal Functions to normalise processing

Version 1.0

Purpose:
Generic, regularly used code is stored within this file.

Usage Instructions:
Not to be confused with toolset_main, this file is intended to store generic functions and classes
for reference elsewhere in the application. Changes to this file are likely to be uncommon

"""

import json
import random

import xml.dom.minidom
import xml.sax.saxutils

from . import test_handler

from .supporting import six

#from splunklib import six
#import splunklib.client as client
#from splunklib.modularinput.event import Event
#from splunklib.modularinput.event_writer import EventWriter

# from splunklib.modularinput import *

# GENERIC FUNCTIONS
class WriteEvent():
    """
    Purpose:
    Centralize event writing.

    Functions externally referenced:
    - write_out_event: write data to event
    - close_event_write: close event writer stream

    """

    def __init__(self, event, event_write):
        self.event = event
        self.event_write = event_write

    def reset_event(self):
        """
        Purpose:
        reset variables within class

        INPUTS:
        - None

        """
        self.event.data = None
        self.event.done = True
        self.event.host = None
        self.event.index = None
        self.event.source = None
        self.event.sourceType = None
        self.event.stanza = None
        self.event.time = None
        self.event.unbroken = True

    def write_out_event(self, inputs, event, sourcetype, timestamp):
        """
        Purpose:
        Writes event to splunk

        INPUTS:
        - event_write: connection to EventWriter
        - inputs: namedTuple built with values from inputs
        - event: json containing data to write to index
        - sourcetype: sourcetype to use
        - timestamp: timestamp to apply to event

        """

        # Build event tuple
        """
        event = Event( \
            data=json.dumps(event), \
            host=inputs.host, \
            index=inputs.index, \
            source=inputs.source, \
            sourcetype=sourcetype, \
            time=timestamp)
        """

        self.reset_event()

        self.event.data = json.dumps(event)
        self.event.host = inputs.host
        self.event.index = inputs.index
        self.event.source = inputs.source
        self.event.sourceType = sourcetype
        self.event.time = timestamp

        self.event_write.write_event(self.event)

        return "OK"

    def close_event_write(self):
        """
        Purpose:
        Closes event writing stream

        INPUTS:
        - None

        """
        self.event_write.close()

def auth_logger(exec_logger, log_event, msg_id, level, msg_text):
    """
    Purpose:
    Initial point of reference for logging.

    INPUTS:
    - exec_logger: logger class reference
    - log_event: dict of fields to be logged out
    - msg_id: Hardcoded reference for clearly indicating source of log message
    - level: DEBUG/INFO/WARN/ERROR/CRIT
    - msg_text: Plain text used to describe log event

    NOTE: This is heavily referenced within the application as a gateway to all logging functions
    """
    logging_data = {}
    logging_data['msg_id'] = msg_id
    logging_data['level'] = level
    logging_data['function'] = log_event['function_name']
    logging_data['file'] = log_event['file']
    logging_data['msg_text'] = msg_text

    # Optional Logging Data
    if "source" in log_event:
        logging_data['source'] = log_event['source']

    if "sourcetype" in log_event:
        logging_data['sourcetype'] = log_event['sourcetype']

    if "host" in log_event:
        logging_data['host'] = log_event['host']

    if "index" in log_event:
        logging_data['index'] = log_event['index']

    if 'loop_count' in log_event and log_event['loop_count'] is not None:
        logging_data['loop_count'] = log_event['loop_count']

    if "data" in log_event:
        logging_data['data'] = log_event['data']

    # Call log_msg function to complete logging process
    exec_logger.log_msg(logging_data)


class InternalLogging():
    """
    Purpose:
    Centralize logging process.

    Functions externally referenced:
    - log_msg: only by auth_logger

    """

    def __init__(self, event_writer, toolset_functions):

        self.log_id = 0
        self.run_id = random.randint(1, 9999999) #nosec
        self.toolset_functions = toolset_functions
        #self.event_write = EventWriter()
        self.event_write = event_writer
        self.loop_id = 0

    #def log_msg(self, msg_id, level, func_name, msg, input_data=None):

    def enrich_log_data(self, input_data):
        """
        Purpose:
        Add extra data to log event.

        INPUTS:
        - input_data: logging data to write out

        """
        input_data['log_id'] = self.log_id
        input_data['run_id'] = self.run_id
        input_data['ta_id'] = self.toolset_functions.get_app_name()

        if self.loop_id > 0:
            input_data['loop_id'] = self.loop_id

        return input_data

    def log_msg(self, input_data):
        """
        Purpose:
        Final logging function.

        INPUTS:
        - input_data: logging data to write out

        NOTE: This should not be referenced directly outside of auth_logger.
        """

        # Temporaru increase of logging level for development purposes
        if input_data['level'] == "DEBUG":
            input_data['level'] = "INFO"

        # Set logging id. used for counting number of log messages for a particular run
        self.log_id += 1

        # Enrich logging data
        input_data = self.enrich_log_data(input_data)

        # Write out to _internal
        self.event_write.log(input_data['level'], "westy_ta_log=" + json.dumps(input_data))

class InternalFunctions():
    """
    Purpose:
    Storage of most generic internal functions.

    Functions externally referenced:
    - All

    """

    def __init__(self, config_str, exec_logger):

        self.config_str = config_str
        self.exec_logger = exec_logger

        self.logging = {}
        self.logging['file'] = "internal"
        self.logging['function_name'] = ""

    '''
    def create_session(self):
        """
        Purpose:
        Used to create a connection with Splunk.

        INPUTS:
        - NONE

        NOTE: Uses self variables for info to handle connection
        """

        # Initial Logging
        self.logging['function_name'] = "create_session"

        auth_logger(self.exec_logger, self.logging, "START", 'DEBUG', "Starting Function")

        # parse the config XML
        doc = xml.dom.minidom.parseString(self.config_str)

        root = doc.documentElement

        # extract the session key
        session_key = root.getElementsByTagName("session_key")[0]

        # Prepare connection
        args = {'token':session_key.firstChild.data, "app":toolset_main.get_app_name()}

        auth_logger(self.exec_logger, self.logging, "RETCON", 'DEBUG', "Returning With Connection")

        # connect and return
        return client.connect(**args)
    '''
    def get_stanza(self):
        """
        Purpose:
        Used to get input stanza.

        INPUTS:
        - NONE

        """

        self.logging['function_name'] = "get_stanza"
        auth_logger(self.exec_logger, self.logging, "START", 'DEBUG', "Starting Function")

        # parse the config XML
        doc = xml.dom.minidom.parseString(self.config_str)
        root = doc.documentElement

        conf_node = root.getElementsByTagName("configuration")[0]
        if conf_node:
            stanza = conf_node.getElementsByTagName("stanza")[0]
            if stanza:

                auth_logger(
                    self.exec_logger,
                    self.logging,
                    "RETSTANZA", 'DEBUG', "Returning Stanza")

                return stanza

        auth_logger(
            self.exec_logger,
            self.logging,
            "UNKNSTANZA", 'WARN', "Returning Unknown Stanza")

        return "Unknown"

    def get_stanza_name(self):
        """
        Purpose:
        Return full input stanza name from config

        INPUTS:
        - NONE

        """

        # Initial Logging
        self.logging['function_name'] = "get_stanza_name"
        auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Get Stanza
        stanza = self.get_stanza()
        self.logging['function_name'] = "get_stanza_name"   # Reinitilize logging

        # Get name of stanza
        stanza_name = stanza.getAttribute("name")
        if stanza_name:
            auth_logger(
                self.exec_logger,
                self.logging,
                "RETSTANZA", 'DEBUG', "Returning Stanza Name")

            return stanza_name

        auth_logger(
            self.exec_logger,
            self.logging,
            "UNKNSTANZA", 'WARN', "Getting Stanza Name")

        return "Unknown"

    def get_split_stanza(self):
        """
        Purpose:
        Splits values in input stanza

        INPUTS:
        - NONE

        """

        # Initialize Logging
        self.logging['function_name'] = "get_split_stanza"
        auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Get Full Stanza
        stanza_name = self.get_stanza_name()

        # Split and return stanza
        return stanza_name.split("://")

    def get_input_from_conf(self, attr_name):
        """
        Purpose:
        Gets a user input value from configuration

        INPUTS:
        - attr_name: field name to extract

        """

        # Initialize Logging
        self.logging['function_name'] = "get_input_from_conf"

        logging_val = {}
        logging_val['attr_name'] = attr_name
        self.logging['data'] = json.dumps(logging_val)
        auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Get stanza from config
        stanza = self.get_stanza()
        self.logging['function_name'] = "get_input_from_conf"   # Reset logging

        # Get stanza name and process
        stanza_name = stanza.getAttribute("name")
        if stanza_name:

            # Special handler for input name extraction
            if attr_name == "stanza_name_split":

                logging_val = {}
                logging_val['stanza_name_split'] = stanza_name.rsplit("://", 1)[1]
                self.logging['data'] = json.dumps(logging_val)
                auth_logger(
                    self.exec_logger,
                    self.logging,
                    "RETSTANZA", 'DEBUG', "Returning Stanza Name")
                self.logging['data'] = None

                return stanza_name.rsplit("://", 1)[1]

            # Special handler for input stanza extraction
            if attr_name == "stanza_name_full":
                logging_val = {}
                logging_val['stanza_name_full'] = stanza_name
                self.logging['data'] = json.dumps(logging_val)
                auth_logger(
                    self.exec_logger,
                    self.logging,
                    "RETSTANZA", 'DEBUG', "Returning Stanza Name")
                self.logging['data'] = json.dumps(logging_val)

                return stanza_name

            # Get input parameters
            params = stanza.getElementsByTagName("param")

            # Loop through param values
            self.exec_logger.loop_id = 0
            for param in params:
                self.exec_logger.loop_id += 1

                logging_val = {}
                logging_val['param_name'] = param.getAttribute("name")
                logging_val['attr_name'] = attr_name
                self.logging['data'] = json.dumps(logging_val)
                auth_logger(
                    self.exec_logger,
                    self.logging,
                    "CHECKVAL", 'DEBUG', "Checking Value")
                self.logging['data'] = None

                # Confirm param in loop is param we're after
                param_name = param.getAttribute("name")
                if param_name and \
                   param.firstChild and \
                   param.firstChild.nodeType == param.firstChild.TEXT_NODE and \
                   param_name == attr_name:

                    logging_val = {}
                    logging_val['field'] = attr_name
                    logging_val['value'] = param.firstChild.data
                    self.logging['data'] = json.dumps(logging_val)
                    auth_logger(
                        self.exec_logger,
                        self.logging,
                        "RETDATA", 'DEBUG', "Returning Data")
                    self.logging['data'] = None
                    self.exec_logger.loop_id = 0

                    # Return value of input
                    return param.firstChild.data

        # Only get here if there's a problem
        self.exec_logger.loop_id = 0
        auth_logger(
            self.exec_logger,
            self.logging,
            "RETUNKNOWN", 'WARN', "Returning Unknown Value")
        return "Unknown"

class ConfHandler():
    """
    Purpose:
    Handle interaction with conf files outside splunklib.

    Functions externally referenced:
    - All

    """

    def __init__(self, service, exec_logger):
        self.service = service
        self.exec_logger = exec_logger

        self.logging = {}
        self.logging['file'] = "internal"
        self.logging['function_name'] = ""

    def create(self, argv):
        """
        Purpose:
        Creates a stanza in conf file

        INPUTS:
        - argv: array with up to 3 values
        -- 1: Name of conf file
        -- 2: Name of stanza
        -- 3: Name of KV Pair

        Minimum requirement is for first 2 values to be populated

        LOGIC:
        if argv doesnt contain a KV pair, a stanza will be created
        if argv contains a KV pair, a stanza in conf file is assumed

        """

        #Initialize logging
        self.logging['function_name'] = "create"

        logging_val = {}
        logging_val['args'] = str(argv)
        self.logging['data'] = json.dumps(logging_val)
        auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        self.logging['data'] = None

        # Assess values of args to understand processing logic
        count = len(argv)

        # unflagged arguments are conf, stanza, key. In this order
        # however, we must have a conf and stanza.
        '''
        cpres = True if count > 0 else False
        spres = True if count > 1 else False
        kpres = True if count > 2 else False
        '''
        cpres = test_handler.test_gt_value(count, 0)
        spres = test_handler.test_gt_value(count, 1)
        kpres = test_handler.test_gt_value(count, 2)

        # Validate against minumum input requirements
        if not cpres and not spres:
            return "Conf name and stanza name is required for create"

        # Initial value assignment
        name = argv[0]
        stan = argv[1]
        conf = self.service.confs[name]

        # Create stanza
        if not kpres:
            conf.create(stan)
            return "OK"

        # Validation for KV Pairs
        kvpair = argv[2].split("=")
        if len(kvpair) != 2:
            return "Creating a k/v pair requires key and value"

        # create key/value pair under existing stanza
        stanza = conf[stan]
        key, value = kvpair
        stanza.submit({key: value})

        return "OK"

    def update(self, argv):
        """
        Purpose:
        Update an existing conf stanza

        INPUTS:
        - argv: array with 3 values
        -- 1: Name of conf file
        -- 2: Name of stanza
        -- 3: Name of KV Pair

        """

        # Initial Logging
        self.logging['function_name'] = "update"

        auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Assess values of args to understand processing logic
        count = len(argv)

        # unflagged arguments are conf, stanza, key. In this order
        # however, we must have a conf and stanza.
        """
        cpres = True if count > 0 else False
        spres = True if count > 1 else False
        kpres = True if count > 2 else False
        """
        kpres = test_handler.test_gt_value(count, 2)

        # Validation
        if not kpres:

            auth_logger(
                self.exec_logger,
                self.logging,
                "PARAMFAIL", 'ERROR', "Invalid Call. key value must be passed through")
            return

        # Setup vars
        name = argv[0]
        stan = argv[1]
        kval = argv[2]

        logging_val = {}
        logging_val['file'] = name
        logging_val['Stanza'] = stan
        self.logging['data'] = json.dumps(logging_val)
        auth_logger(
            self.exec_logger,
            self.logging,
            "UPDSTART", 'DEBUG', "Start to update stanza")
        self.logging['data'] = None

        # Update Block
        try:
            # get conf
            conf = self.service.confs[name]
            stanza = conf[stan]

            # update
            stanza.update(**kval)
            stanza.refresh()
            auth_logger(
                self.exec_logger,
                self.logging,
                "UPDOK", 'DEBUG', "Stanza Updated")
        except ImportError:

            logging_val = {}
            logging_val['Exception'] = Exception("Failed to update conf file")
            self.logging['data'] = json.dumps(logging_val)
            auth_logger(
                self.exec_logger,
                self.logging,
                "UPDFAIL", 'ERROR', "Invalid Call. key value must be passed through")

            self.logging['data'] = None

    def list(self, argv):
        """
        Purpose:
        List all conf files or values within

        INPUTS:
        - argv: array with 3 values
        -- 1: Name of conf file
        -- 2: Name of stanza
        -- 3: Name of KV Pair

        """

        # Initial Logging
        self.logging['function_name'] = "list"

        auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Initial analysis of input
        count = len(argv)

        # unflagged arguments are conf, stanza, key. In this order
        # but all are optional
        cpres = test_handler.test_gt_value(count, 0)
        spres = test_handler.test_gt_value(count, 1)
        kpres = test_handler.test_gt_value(count, 2)

        config = {}

        if not cpres:
            # List out the available confs
            for conf in self.service.confs:
                config['file'] = conf.name

            return config

        # Print out detail on the requested conf
        # check for optional stanza, or key requested (or all)
        name = argv[0]
        conf = self.service.confs[name]

        config['file'] = conf.name

        logging_val = {}
        logging_val['name'] = name
        logging_val['conf'] = str(conf)

        self.logging['data'] = json.dumps(logging_val)
        auth_logger(
            self.exec_logger,
            self.logging,
            "STLOOP", 'DEBUG', "Starting Loop")
        self.logging['data'] = None
        config = {}

        for stanza in conf:
            if (spres and argv[1] == stanza.name) or not spres:

                config['stanza'] = stanza.name
                for key, value in six.iteritems(stanza.content):
                    if (kpres and argv[2] == key) or not kpres:
                        config[key] = value

        self.logging['data'] = json.dumps(config)
        auth_logger(
            self.exec_logger,
            self.logging,
            "RETURN", 'DEBUG', "Returning From Function")
        self.logging['data'] = None

        return config
