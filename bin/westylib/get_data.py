"""
Data Collector

Version 1.0

Purpose:
Initialization and central function for data collection.

Usage Instructions:
Please leave this as is.
Changes for data collection should be handled within toolset_main where possible

"""

import json

from . import creds, internal, kv_collection

class GetDataMain():
    """Main class to handle retrieval of data"""

    def __init__(self, config_str, event_classes, splunk_service, toolset):

        self.exec_logger = event_classes['logger']
        self.logging = {}
        self.logging['file'] = "get_data"
        self.logging['function_name'] = "__init__"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # connect to internal functions
        self.internal_functions = internal.InternalFunctions(config_str, self.exec_logger)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CONSERV", 'DEBUG', "Connecting Splunk Service")

        # connect to Splunk
        #self.splunk_service = self.internal_functions.create_session()
        self.splunk_service = splunk_service

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "INITCREDS", 'DEBUG', "Initializing Credentials")

        # connect to credential service
        self.credentials = creds.CredentialHandler(self.splunk_service,
                                                   self.exec_logger,
                                                   toolset['functions'])

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "INITKV", 'DEBUG', "Initializing KV Function")

        # connect to KV Store
        self.kv_store = kv_collection.KVHandler(self.splunk_service,
                                                self.exec_logger,
                                                toolset['main'])

        # connect to toolset data class
        self.toolset = toolset['main']

    def main(self, config_str):
        """
        Purpose:
        Main routine to control processing of input.

        INPUTS:
        - config_str: From stdin

        NOTE: This is the main central function and SHOULT NOT be edited
        """

        # Initial logging
        self.logging['function_name'] = "main"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'INFO', "Starting Collection")

        # Get inputs
        inputs = self.get_inputs(config_str)
        self.logging['function_name'] = "main" # reset function name

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "KVCALL", 'DEBUG', "Getting Checkpoint Time")

        # Get lastrun time from KV Store
        checkpoint_time = self.kv_store.get_checkpoint( \
            inputs.source)
        self.logging['function_name'] = "main" # reset function name


        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GETEVCALL", 'DEBUG', "Get Toolset Events Call")

        # Get events and write to Splunk
        cur_status = self.toolset.get_events(
            inputs,
            checkpoint_time,
            self.kv_store.vars['timestamp'])

        if cur_status != "OK":
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETERROR", 'DEBUG', "Error Ocurred")
        else:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "SVKV", 'DEBUG', "Save checkpoint")

            # Store current runtime to KV Store
            checkpoint_time = self.kv_store.save_checkpoint( \
                inputs.source)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "FIN", 'INFO', "Finish Collection")

        return "OK"

    def get_inputs(self, config_str):
        """
        Purpose:
        Main routine to collect input values.

        INPUTS:
        - config_str: From stdin

        NOTE:
        Any toolset specific changes should be directed to supporting
        functions in toolset_main

        """

        # Initial logging
        self.logging['function_name'] = "get_inputs"

        logging_val = {}
        logging_val['config_str'] = config_str
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Call function to get input values from stdin
        self.toolset.get_inputs()

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "SETTUPLE", 'DEBUG', "Setting Tuple")

        # set inputs tuple used throughout application
        input_values = self.toolset.set_inputs_tuple(config_str)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CHECKAUTH", 'DEBUG', "Checking Key")

        # Get credentials from cred lib
        self.credentials.get_creds(input_values)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "SETFINTUPLE", 'DEBUG', "Assigning Final Tuple")

        # Set inputs tuple again with updated creds values
        input_values = self.toolset.set_inputs_tuple(config_str)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETURNINPUTVALS", 'DEBUG', "Return input vals")

        return input_values
