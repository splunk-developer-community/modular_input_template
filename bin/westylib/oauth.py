"""
OAuth 2.0 Handler

Version 1.0

Purpose:
Handles all specifc processing required with OAuth2.0 authentication.

NOTE: Toolsets have slightly varying approaches to this auth method. Any specific changes need to be
handled within the toolset_main file.

"""

import json
from .supporting import requests
from .supporting.requests.exceptions import HTTPError
from . import internal, creds


class AuthHandler():
    """
    Purpose:
    Class to handle OAuth2.0 processing.

    Functions externally referenced:
    - main_auth: generate new access token

    """

    def __init__(self, splunk_service, exec_logger, toolset):

        # Store Connections
        self.splunk_service = splunk_service
        self.exec_logger = exec_logger
        self.creds = creds.CredentialHandler(self.splunk_service, self.exec_logger, toolset)
        self.toolset = toolset

        self.logging = {}
        self.logging['file'] = "oauth"
        self.logging['function_name'] = ""
        self.logging['loop_count'] = None

        # Store class specific variables
        self.vars = {}
        self.vars['separator'] = "``westy_auth_sep``"
        self.vars['refresh'] = "refresh_code"
        self.vars['access'] = "access_code"

    def main_auth(self, inputs):
        """
        Purpose:
        Generate refresh token

        INPUTS:
        - inputs: namedTuple containing input fields

        """

        # Initialize Logging
        self.logging['function_name'] = "main_auth"
        self.logging['source'] = inputs.source
        self.logging['host'] = inputs.host
        self.logging['index'] = inputs.index

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Get access token from cred lib
        access_token = self.creds.password_request("GET", inputs, extra="auth")

        # Build dataset for request
        request = {}
        request['key'] = self.creds.password_request("GET", inputs, extra="key")
        request['secret'] = self.creds.password_request("GET", inputs, extra="secret")
        request['callback_url'] = inputs.callback_url
        request['grant_type'] = "authorization_code" \
            if access_token == "Unknown" \
            else "refresh_token"
        request['token_type'] = "code" \
            if access_token == "Unknown" \
            else "refresh_token"
        request['token_val'] = self.creds.password_request("GET", inputs, extra="code") \
            if access_token == "Unknown" \
            else self.creds.password_request("GET", inputs, extra="refresh")

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CALLAUTH", 'DEBUG', "Calling API")

        # Run function to call API
        status_code, response = self.call_api(request)
        self.logging['function_name'] = "main_auth"     # Reset logging

        # Return error if response is not 200
        if status_code != 200:
            return "RETERROR"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "STORETOK", 'DEBUG', "Storing Token")

        # Call function to store tokens in response
        self.store_access_token(inputs, response)
        self.logging['function_name'] = "main_auth"
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETTOK", 'DEBUG', "Returning Token")
        return response['access_token']

    def call_api(self, request):
        """
        Purpose:
        Interact with auth token endpoint

        INPUTS:
        - request: JSON data passed to API

        """

        # Initialize Logging
        self.logging['function_name'] = "call_api"

        logging_val = {}
        logging_val['grant_type'] = request['grant_type']
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Build url
        url = self.toolset.get_root_url() + "/oauth/token"

        # Build parameters for call
        parameters = {
            "client_id" : str(request['key']),
            "client_secret" : str(request['secret']),
            str(request['token_type']) : str(request['token_val']),
            "grant_type" : logging_val['grant_type'],
            "redirect_uri" : str(request['callback_url'])}

        logging_val = {}
        logging_val['url'] = url
        logging_val['token_type'] = request['token_type']
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CALLAPI", 'DEBUG', "Calling API")
        self.logging['data'] = None

        # call API to request token
        try:
            response = requests.post(url, params=parameters)

            # If the response was successful, no Exception will be raised
            response.raise_for_status()

            logging_val = {}
            logging_val['url'] = url
            logging_val['response_code'] = response.status_code
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETAPI", 'DEBUG', "API Return")
            self.logging['data'] = None

            r_json = response.json()

        # Error handling
        except HTTPError as http_err:
            logging_val = {}
            logging_val['url'] = url
            logging_val['response_code'] = response.status_code
            logging_val['error'] = str(http_err)
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "FAILHTTPERR", 'ERROR', 'HTTP error occurred')
            self.logging['data'] = None
            return response.status_code, None
        """
        except Exception as err:
            logging_val = {}
            logging_val['url'] = url
            logging_val['response_code'] = response.status_code
            logging_val['error'] = err
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "FAILEXCEPERR", 'ERROR', 'Other error occurred')
            self.logging['data'] = None
            return response.status_code, None
        """
        # Return
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "FUNCRET", 'DEBUG', "Function Return OK")

        return response.status_code, r_json

    def store_access_token(self, inputs, response):
        """
        Purpose:
        Store tokens into cred lib

        INPUTS:
        - inputs: namedTuple containing input fields
        - response: JSON data returned from API call

        """

        # Initialize Logging
        self.logging['function_name'] = "store_access_token"
        self.logging['source'] = inputs.source
        self.logging['host'] = inputs.host
        self.logging['index'] = inputs.index

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # use creds to store both access and refresh tokens

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CRPREQAUTH", 'DEBUG', "Updating Password: AUTH")

        # Store Access Token
        cred_status = self.creds.password_request( \
            "UPDATE", \
            inputs, \
            password=response["access_token"], \
            extra="auth"
            )

        if cred_status == "Unknown":

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETPASSFAILAUTH", 'WARN', "Returning with password fail from AUTH")

            return "Failed to update passwords"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CRPREQREF", 'DEBUG', "Updating Password: REFRESH")

        # Store Refresh Token
        cred_status = self.creds.password_request( \
            "UPDATE", \
            inputs, \
            password=response["refresh_token"], \
            extra="refresh")

        if cred_status == "Unknown":
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETPASSFAILREFRESH", \
                'WARN', \
                "Returning with password fail from Refresh")

            return "Failed to update passwords"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETPASSOK", \
            'DEBUG', \
            "Returning OK")

        return "OK"
