"""
Credential Handler

Version 1.0

Purpose:
Handle interactions with internal splunk storage passwords module.

Usage Instructions:
Entry functions are password_request and get_creds.

"""

import json
from . import internal

class CredentialHandler():
    """Central class handling integrations with credential endpoints"""

    def __init__(self, splunk_service, exec_logger, toolset_functions):

        # Pre-Initialised connections
        self.splunk_service = splunk_service
        self.exec_logger = exec_logger
        self.toolset = toolset_functions

        # Logging Values
        self.logging = {}
        self.logging['file'] = "creds"
        self.logging['function_name'] = ""

        # Central Values Used Throughout
        self.var = {}
        self.var['credential'] = "``westy_cred_sep``"
        self.var['mask'] = "********"

        # Handle credentials
        self.credentials = {}
        self.credentials['key'] = ""
        self.credentials['secret'] = ""
        self.credentials['code'] = ""

    def password_request(self, function, inputs, password=None, extra=None):
        """
        Purpose:
        Main function to interact with storage_passwords.

        INPUTS:
        - function: Must be one of the following values
            -- GET
            -- CREATE
            -- UPDATE
            -- DELETE
        - inputs: namedTuple set after collecting input Values
        - password: Only populated when function is CREATE / UPDATE. Value must be password to store
        - extra: Associated tag to append to stored username.
        """

        # Initial Logging
        self.logging['function_name'] = "password_request"
        self.logging['source'] = inputs.source
        self.logging['host'] = inputs.host
        self.logging['index'] = inputs.index

        logging_val = {}
        logging_val['function'] = function
        if extra is not None:
            logging_val['extra'] = extra
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Get value to store against username
        cred_id = self.get_identifier(inputs.auth, extra)

        # Function is GET
        if function == "GET":

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "GETSTART", 'DEBUG', "Starting GET")

            # Retrieve the password from the storage/passwords endpoint
            for storage_password in self.splunk_service.storage_passwords:
                if storage_password.username == cred_id:

                    internal.auth_logger(
                        self.exec_logger,
                        self.logging,
                        "RETPASSOK", 'DEBUG', "Returning Password")

                    return storage_password.content.clear_password

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETPASSFAIL", 'WARN', "Unable to find Password")
            return "Unknown"

        # CRUD operations

        if function in ("DELETE", "UPDATE") and \
           self.splunk_service.storage_passwords is not None:

            logging_val = {}
            logging_val['username'] = cred_id
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "DELUPSTART", 'DEBUG', "Start Delete / Update")
            self.logging['data'] = None

            # Retrieve the password from storage/passwords endpoint
            if not str(self.splunk_service.storage_passwords).startswith("*"):

                # If the credential already exists, delte it.
                for storage_password in self.splunk_service.storage_passwords:

                    if storage_password.username == str(cred_id):
                        self.splunk_service.storage_passwords.delete(\
                            username=storage_password.username)
                        break

        if function in ("CREATE", "UPDATE") and \
           password is not None:

            logging_val = {}
            logging_val['username'] = cred_id
            logging_val['password'] = password
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CRUPSTART", 'DEBUG', "Start Create / Update")
            self.logging['data'] = None

            # Create password in lib
            self.splunk_service.storage_passwords.create(password, str(cred_id))

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETURN", 'DEBUG', "Returning OK")

        return "OK"

    def get_creds(self, inputs):
        """
        Purpose:
        Get all input passwords associated with input.

        INPUTS:
        - inputs: namedTuple set after collecting input Values
        """

        # Initial Logging
        self.logging['function_name'] = "get_creds"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Connect to conf handler service to interact with auth conf file
        conf_handler = internal.ConfHandler(self.splunk_service, self.exec_logger)
        args = ["ta_auth", inputs.auth]

        # List inputs held in auth conf file against auth stanza
        values = conf_handler.list(args)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETAUTH", 'DEBUG', "Returned")

        # True only if stanza does not exist
        if values == {}:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CRSTANZA", 'DEBUG', "Creating Stanza")

            # Create Stanza
            args = ["ta_auth", inputs.auth]
            values = conf_handler.create(args)

            # Assign masked values to stanza
            args.append({"key": self.var['mask']})
            conf_handler.update(args)

            args[2] = {"secret": self.var['mask']}
            conf_handler.update(args)

            args[2] = {"code": self.var['mask']}
            conf_handler.update(args)

            # Assign values json object for later use
            values = {}
            values['key'] = self.var['mask']
            values['secret'] = self.var['mask']
            values['code'] = self.var['mask']
        elif inputs.key == "Unknown" \
          or inputs.secret == "Unknown" \
          or inputs.code == "Unknown":
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CREDSETST", 'DEBUG', "Starting Input Set")
            self.update_inputs(inputs, input_only=True)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CREDSETFIN", 'DEBUG', "Finishing Input Set")
            return

        #Initialize flag tracking if passwords need updating
        input_update = False

        #Check Conf
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GOKEYCONF", 'DEBUG', "Getting / Updating Key from Conf")
        self.get_cred(inputs, values, "key", conf_handler)
        self.logging['function_name'] = "get_creds"
        #Check Input
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GOKEYINPUT", 'DEBUG', "Getting / Updating Key from Input")
        input_update = self.input_check(input_update, inputs, "key", inputs.key)
        self.logging['function_name'] = "get_creds"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GOSECCONF", 'DEBUG', "Getting / Updating Secret")
        self.get_cred(inputs, values, "secret", conf_handler)
        self.logging['function_name'] = "get_creds"
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GOSECINPUT", 'DEBUG', "Getting / Updating Secret from Input")
        input_update = self.input_check(input_update, inputs, "secret", inputs.secret)
        self.logging['function_name'] = "get_creds"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GOCODE", 'DEBUG', "Getting / Updating Code")
        self.get_cred(inputs, values, "code", conf_handler)
        self.logging['function_name'] = "get_creds"
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "GOSECINPUT", 'DEBUG', "Getting / Updating Secret from Input")
        input_update = self.input_check(input_update, inputs, "code", inputs.code)
        self.logging['function_name'] = "get_creds"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CHECKCREDS", 'DEBUG', "Checking for Changes")

        #True if password masked inputs have been changed
        if input_update:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CREDUPDATE", 'DEBUG', "Starting Input Update")
            self.update_inputs(inputs)
            self.logging['function_name'] = "get_creds"
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CREDFIN", 'DEBUG', "Finishing Input Update")

    # SUPPORTING FUNCTIONS
    # These functions are only to be called internally within class
    def get_identifier(self, field_id, extra):
        """
        Purpose:
        Ensures username field is consistent.

        INPUTS:
        - field_id: Auth Name
        - extra: Tag appended to username

        NOTE: There's no logging for this function due to its own size
        """
        if extra is None:
            return str(field_id)

        return \
            str(field_id) + \
            self.var['credential'] + \
            extra

    def get_cred(self, inputs, values, field, conf_handler):
        """
        Purpose:
        Get credential and mask within conf file if it is not masked

        INPUTS:
        - inputs: namedTuple of inputs
        - values: JSON containing all credential values in inputs
        - field: name of field to check
        - conf_handler: Connected conf handler class to interact with auth conf
        """

        #Initialize Logging
        self.logging['function_name'] = "get_cred"

        logging_val = {}
        logging_val['param_field'] = field
        logging_val['values'] = str(values)

        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # if input is not masked, ensure that it is updated and value stored in cred lib
        if field in values and values[field] != self.var['mask']:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CREDUPDATE", 'DEBUG', "Updating Credential")

            # update Cred Lib
            self.credentials[field] = values[field]
            self.password_request("UPDATE",
                                  inputs,
                                  password=self.credentials[field],
                                  extra=field.lower())
            self.logging['function_name'] = "get_cred"
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CREDDONE", 'DEBUG', "Credential Updated")

            # Mask input in conf file
            store_val = {field: self.var['mask']}
            args = ["ta_auth", inputs.auth, store_val]

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "CALLDEL", 'DEBUG', "Calling Update")
            conf_handler.update(args)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETDEL", 'DEBUG', "Calling Update")

        # Get cred from cred lib
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CREDGET", 'DEBUG', "Gettting Credential")
        self.credentials[field] = self.password_request("GET", inputs, extra=field.lower())
        self.logging['function_name'] = "get_cred"
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CREDGOT", 'DEBUG', "Credential Got")

    def input_check(self, flag, input_values, field_type, value):
        """
        Purpose:
        Interact with values in input.

        INPUTS:
        - flag: Indicates whether inputs need to be updatd. Only used as a return value
        - input_values: namedTuple set after collecting input Values
        - field_type: name of field to check
        - value: value of field
        """

        # Initial Logging
        self.logging['function_name'] = "input_check"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Start Function")

        # if no value exists, set update flag to true and return
        if value in (None, ""):
            return True

        # if input value is masked, return inputted update status
        if value == self.var['mask']:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "MASKVALUED", 'DEBUG', "Value is Masked")
            return flag

        # if here. value requires storage in cred lib
        logging_val = {}
        logging_val['type'] = field_type
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "MASKVAL", 'DEBUG', "Masking Value")
        self.logging['data'] = None

        self.password_request(
            "UPDATE", \
            input_values, \
            password=value, \
            extra=field_type
        )

        self.logging['function_name'] = "input_check"
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETTRUE", 'DEBUG', "Returning True")

        return True

    def update_inputs(self, inputs, input_only=None):
        """
        Purpose:
        Update values in inputs.

        INPUTS:
        - flag: Indicates whether inputs need to be updatd. Only used as a return value
        - input_values: namedTuple set after collecting input Values
        - field_type: name of field to check
        - value: value of field
        """

        # Initial Logging
        self.logging['function_name'] = "update_inputs"
        self.logging['source'] = inputs.source
        self.logging['host'] = inputs.host
        self.logging['index'] = inputs.index

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Delete refresh and auth tokens
        if input_only is None:
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "DELST", 'DEBUG', "Delete Start")
            self.password_request("DELETE", inputs, extra="auth")
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "DALCON", 'DEBUG', "Delete Continue")
            self.password_request("DELETE", inputs, extra="refresh")
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "DELFIN", 'DEBUG', "Delete Finish")

        # Initialize connection to internal functions
        internal_functions = internal.InternalFunctions(inputs.config, self.exec_logger)

        # Get stanza values
        kind, input_name = internal_functions.get_split_stanza()
        item = self.splunk_service.inputs.__getitem__((input_name, kind))

        # Set values to be stored in inputs
        kwargs = self.toolset.set_inputs(inputs, self.var['mask'])

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "UPDATE", 'DEBUG', "Updating Input")

        # Update inputs.conf
        item.update(**kwargs).refresh()
