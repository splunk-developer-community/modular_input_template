"""
Testing functions to control checks and tests throughout application

Version 1.0

Purpose:
Generic, include any tests that need to be used by the app within this file.

"""

def test_empty_array(value):
    """
    Purpose:
    Returns true if the input array is not empty.

    INPUTS:
    - value: array

    """

    if value != "[]":
        return True
    return False

def test_gt_value(value, condition):
    """
    Purpose:
    Returns true if the input value is greater than the condition.

    INPUTS:
    - value: value to check
    - condition: value to check against

    """
    if value > condition:
        return True
    return False
