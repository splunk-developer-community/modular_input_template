"""
Initial Script

Version 1.0

Purpose:
Initial file called to handle all TA functions.

Usage Instructions:
Changes to this file should reference supporting functions and classes in other files.
Specific logic should be separated from this file to limit the changes required

"""

from __future__ import print_function
import sys

from westylib import get_data, internal
import internal_root
import toolset_main

from splunklib.modularinput.event import Event
from splunklib.modularinput.event_writer import EventWriter

# Get Scheme used in Data Collection UI Screen
SCHEME = toolset_main.get_scheme()

def do_scheme():
    """
    Purpose:
    Introspection Routine.

    INPUTS:
    - NONE

    NOTE: This should not be edited
    """
    print(SCHEME)

# Empty validation routine. This routine is optional.
def validate_arguments():
    """
    Purpose:
    Validation Routine.

    INPUTS:
    - NONE

    NOTE: If required, this should only refer to a class elsewhere.
    No validation logic shoud be written to this function beyond the interaction with that class
    """

    # Replicates pass statement without issues raised from linter
    abc = 1
    abc = abc + 1


# Routine to index data
def run_script():
    """
    Purpose:
    Routine to index data.

    INPUTS:
    - NONE

    NOTE: This is configured to call get_data which is responsible for all management of data
    collection logic. no further changes should be made to this function.
    """

    config_str = sys.stdin.read()

    event_classes = {}
    event_classes['builder'] = Event()
    event_classes['writer'] = EventWriter()
    event_classes['logger'] = internal.InternalLogging(EventWriter(),
                                                       toolset_main.ToolsetFunctions())
    internal_functions = internal.InternalFunctions(config_str,
                                                    event_classes['logger'])
    integration_functions = internal_root.IntegrationFunctions(config_str, event_classes['logger'])
    splunk_service = integration_functions.create_session()

    toolset = {}
    toolset['functions'] = toolset_main.ToolsetFunctions()
    toolset['main'] = toolset_main.Toolset(splunk_service,
                                           event_classes['logger'],
                                           internal_functions,
                                           event_classes)

    collect_data = get_data.GetDataMain(config_str, event_classes, splunk_service, toolset)
    collect_data.main(config_str)

# Script must implement these args: scheme, validate-arguments
if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == "--scheme":
            do_scheme()
        elif sys.argv[1] == "--validate-arguments":
            validate_arguments()
        else:
            pass
    else:
        run_script()

    sys.exit(0)
