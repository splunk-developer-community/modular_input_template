"""
Toolset specific file

Version 1.0

Purpose:
Handles all toolset specific processing.

"""

import json
import datetime
from collections import namedtuple

from westylib import oauth, creds, internal
from westylib.supporting import requests
from westylib.supporting.requests.exceptions import HTTPError
'''
import requests
from requests.exceptions import HTTPError

import oauth
import creds
import internal
'''
# from splunklib.modularinput.event_writer import EventWriter

def get_scheme():
    """
    Purpose:
    Return input scheme

    INPUTS:
    - None

    """
    return """<scheme>
        <title>GitLab</title>
        <description>Get data from GitLab.</description>
        <use_external_validation>true</use_external_validation>
        <streaming_mode>xml</streaming_mode>
        <endpoint>
            <args>
                <arg name="name">
                    <title>GitLab Project Name</title>
                    <description>Name of the current project name</description>
                </arg>
                <arg name="group_type">
                    <title>Project ID</title>
                    <description>Group or Project</description>
                </arg>
                <arg name="project_id">
                    <title>Group/Project ID</title>
                    <description>Your GitLab Group/Project ID</description>
                </arg>
                <arg name="auth">
                    <title>Auth</title>
                    <description>Name of Auth Scheme</description>
                </arg>
                <arg name="key">
                    <title>Key</title>
                    <description>Auth Key</description>
                    <required_on_create>False</required_on_create>
                </arg>
                <arg name="secret">
                    <title>Secret</title>
                    <description>Auth Secret</description>
                    <required_on_create>False</required_on_create>
                </arg>
                <arg name="code">
                    <title>Code</title>
                    <description>Auth Code</description>
                    <required_on_create>False</required_on_create>
                </arg>
                <arg name="callback_url">
                    <title>Callback Url</title>
                    <description>Your configured Callback URL</description>
                </arg>
                <arg name="scope">
                    <title>Scope</title>
                    <description>Application Scope</description>
                </arg>
            </args>
        </endpoint>
    </scheme>
    """

def call_api(method, url, headers, params, data=None):
    """
    Purpose:
    Integration with requests module for API calls

    INPUTS:
    - method: get/post
    - url: full url to call
    - headers: headers for request
    - params: params for request
    - data: only required for POST requests

    """
    response = ""

    if method == "get":

        response = requests.get(
            url,
            headers=headers,
            params=params,
            )
    elif method == "post":
        response = requests.post(
            url,
            headers=headers,
            params=params,
            data=data
            )

    return response

class Toolset():
    """
    Purpose:
    Toolset function integration.

    Functions externally referenced:
    - ALL

    """

    def __init__(self, splunk_service, exec_logger, internal_functions, event):
        self.splunk_service = splunk_service
        self.exec_logger = exec_logger
        self.internal_functions = internal_functions
        self.input_class = ToolsetInputs(self.internal_functions, self.exec_logger)
        self.data_class = ToolsetData(self.splunk_service,
                                      self.exec_logger,
                                      self.internal_functions,
                                      event)
        self.function_class = ToolsetFunctions()

    # Functions Mapping
    def get_app_name(self):
        """
        Purpose:
        Call get_app_name function in the function class

        """

        return self.function_class.get_app_name()

    def get_collection_name(self):
        """
        Purpose:
        Call get_collection_name function in the function class

        INPUTS:
        - None

        """
        return self.function_class.get_collection_name()

    def get_auth_conf_file_name(self):
        """
        Purpose:
        Call get_auth_conf_file_name function in the function class

        INPUTS:
        - None

        """
        return self.function_class.get_auth_conf_file_name()

    def set_inputs(self, inputs, mask):
        """
        Purpose:
        Call set_inputs function in the function class

        INPUTS:
        - inputs: namedTuple containing values from inputs
        - mask: Value used to replace sensitive info

        """
        return self.function_class.set_inputs(inputs, mask)

    def get_inputs(self):
        """
        Purpose:
        Call get_inputs function in the input class

        """

        self.input_class.get_inputs()

    def set_inputs_tuple(self, config_str):
        """
        Purpose:
        Call set_inputs_tuple function in the input class

        """

        return self.input_class.set_inputs_tuple(config_str)

    # Data Mapping
    def get_events(self, inputs, lastrun, currun):
        """
        Purpose:
        Call get_events function in the data class

        """

        return self.data_class.get_events(inputs, lastrun, currun)

class ToolsetFunctions():
    """
    Purpose:
    Toolset function collections.

    Functions externally referenced:
    - ALL

    """

    def __init__(self):

        self.dataset = {}
        self.dataset['url_root'] = "https://gitlab.com"
        self.dataset['api_version'] = "v4"
        self.dataset['name_app'] = "TA-gitlab"
        self.dataset['name_collection'] = "gitlab_ta"
        self.dataset['name_auth_conf'] = "ta_auth"
        self.dataset['mask'] = ""
        self.dataset['auth_code'] = ""

    def set_inputs(self, inputs, mask):
        """
        Purpose:
        Return input scheme

        INPUTS:
        - inputs: namedTuple containing values from inputs
        - mask: Value used to replace sensitive info

        """

        self.dataset['mask'] = mask

        return {
            "group_type": str(inputs.type),
            "project_id": str(inputs.id),
            "auth": str(inputs.auth),
            "key": self.dataset['mask'],
            "secret": self.dataset['mask'],
            "code": self.dataset['mask'],
            "callback_url": str(inputs.callback_url),
            "scope": str(inputs.scope)
        }

    def get_root_url(self):
        """
        Purpose:
        Return root URL

        INPUTS:
        - None

        """
        return self.dataset['url_root']

    def get_url(self, inputs):
        """
        Purpose:
        Return root url used for all data api calls

        INPUTS:
        - inputs: namedTuple containing values from inputs

        """
        return self.get_root_url() + \
               '/api/' + self.get_api_version() + \
               '/' + inputs.type + \
               '/' + inputs.id

    def get_api_version(self):
        """
        Purpose:
        Return API version used for application

        INPUTS:
        - None

        """
        return self.dataset['api_version']

    def get_headers(self, auth_code):
        """
        Purpose:
        Build header for data api calls

        INPUTS:
        - auth_code: auth code from cred lib

        """
        self.dataset['auth_code'] = auth_code
        return {'Authorization' : '{}'.format('Bearer ' + str(self.dataset['auth_code']))}



    def get_app_name(self):
        """
        Purpose:
        Return application name for logging purposes

        INPUTS:
        - None
        """
        return self.dataset['name_app']

    def get_collection_name(self):
        """
        Purpose:
        Return application name to be used in KV Store

        INPUTS:
        - None

        """
        return self.dataset['name_collection']

    def get_auth_conf_file_name(self):
        """
        Purpose:
        Returns name of auth conf file

        INPUTS:
        - None

        """
        return self.dataset['name_auth_conf']


class ToolsetInputs():
    """
    Purpose:
    Toolset specific inputs.

    Functions externally referenced:
    - get_inputs: Get values from conf file
    - set_inputs_tuple: Set namedTuple used throughout the scrupt

    """

    def __init__(self, internal_functions, exec_logger):
        # Tuple definition for inputs
        self.tuple = "\
            config \
            stanza \
            type \
            id \
            auth \
            key \
            secret \
            code \
            callback_url \
            scope \
            source \
            host \
            index"

        self.internal_functions = internal_functions
        self.exec_logger = exec_logger

        # Define default values for inputs
        self.inputs = {}
        self.inputs['stanza_name'] = ""
        self.inputs['group_type'] = ""
        self.inputs['project_id'] = ""
        self.inputs['auth'] = ""
        self.inputs['key'] = ""
        self.inputs['secret'] = ""
        self.inputs['code'] = ""
        self.inputs['callback_url'] = ""
        self.inputs['scope'] = ""

        self.logging = {}
        self.logging['file'] = "toolset_main"
        self.logging['function_name'] = ""

    def get_inputs(self):
        """
        Purpose:
        Control collection of values from inputs

        INPUTS:
        - None

        """

        # Logging Initialization
        self.logging['function_name'] = "get_inputs"


        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Get values from conf file
        self.inputs['stanza_name'] = \
            self.internal_functions.get_input_from_conf(attr_name="stanza_name_split")
        self.inputs['group_type'] = \
            self.internal_functions.get_input_from_conf(attr_name="group_type")
        self.inputs['project_id'] = \
            self.internal_functions.get_input_from_conf(attr_name="project_id")
        self.inputs['auth'] = \
            self.internal_functions.get_input_from_conf(attr_name="auth")
        self.inputs['key'] = \
            self.internal_functions.get_input_from_conf(attr_name="key")
        self.inputs['secret'] = \
            self.internal_functions.get_input_from_conf(attr_name="secret")
        self.inputs['code'] = \
            self.internal_functions.get_input_from_conf(attr_name="code")
        self.inputs['callback_url'] = \
            self.internal_functions.get_input_from_conf(attr_name="callback_url")
        self.inputs['scope'] = \
            self.internal_functions.get_input_from_conf(attr_name="scope")
        self.inputs['source'] = \
            self.internal_functions.get_input_from_conf(attr_name="stanza_name_full")
        self.inputs['host'] = \
            self.internal_functions.get_input_from_conf(attr_name="host")
        self.inputs['index'] = \
            self.internal_functions.get_input_from_conf(attr_name="index")

        # Logging Enrichment
        self.logging['source'] = self.inputs['source']
        self.logging['host'] = self.inputs['host']
        self.logging['index'] = self.inputs['index']
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "END", 'DEBUG', "Completed Function OK")

    def set_inputs_tuple(self, config_str):
        """
        Purpose:
        Assign input values from conf file to namedtuple

        INPUTS:
        - config_str: from stdin

        """

        # Initialize Logging
        self.logging['function_name'] = "set_inputs_tuple"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Get tuple definition
        inputs = namedtuple("inputs", self.tuple)

        input_values = inputs( \
            config=config_str, \
            stanza=str(self.inputs['stanza_name']), \
            type=str(self.inputs['group_type']), \
            id=str(self.inputs['project_id']), \
            auth=str(self.inputs['auth']), \
            key=str(self.inputs['key']), \
            secret=str(self.inputs['secret']), \
            code=str(self.inputs['code']), \
            callback_url=str(self.inputs['callback_url']), \
            scope=str(self.inputs['scope']), \
            source=str(self.inputs['source']), \
            host=str(self.inputs['host']), \
            index=str(self.inputs['index']))

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "END", 'DEBUG', "Completed Function OK")

        return input_values

class ToolsetAPI():
    """
    Purpose:
    Toolset specific interaction with API.

    Functions externally referenced:
    - get_data: get data from toolset

    """

    def __init__(self, splunk_service, exec_logger):

        self.splunk_service = splunk_service
        self.exec_logger = exec_logger

        self.auth = oauth.AuthHandler(self.splunk_service, self.exec_logger, ToolsetFunctions())


        self.creds = creds.CredentialHandler(self.splunk_service,
                                             self.exec_logger,
                                             ToolsetFunctions())
        self.function_class = ToolsetFunctions()

        self.logging = {}
        self.logging['file'] = "toolset_main"
        self.logging['function_name'] = ""

    def enrich_logging(self, inputs):
        """
        Purpose:
        Enrich logging data

        INPUTS:
        - inputs: namedTuple built with values from inputs
        """
        self.logging['source'] = inputs.source
        self.logging['host'] = inputs.host
        self.logging['index'] = inputs.index

    def get_data(self, inputs, endpoint, params=None):
        """
        Purpose:
        Generate interaction with toolset API

        INPUTS:
        - inputs: namedTuple built with values from inputs
        - endpoint: Specific endpoint to call
        - params: extra params required for request
        """

        #Initialize logging
        self.logging['function_name'] = "get_data"
        self.enrich_logging(inputs)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Get authorization_code stored in cred lib
        auth_code = self.creds.password_request("GET", inputs, extra="auth")

        # HTTP Initialize
        headers = self.function_class.get_headers(auth_code)
        url = self.function_class.get_url(inputs)

        if endpoint != "project":
            url = url + "/" + endpoint

        logging_val = {}
        logging_val['url'] = url
        logging_val['params'] = params if not params is None else None
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CALLAPI", 'DEBUG', "Calling API")
        self.logging['data'] = None

        # Make call to API
        response = call_api(
            "get",
            url,
            headers,
            params
            )

        logging_val = {}
        logging_val['response_code'] = response.status_code
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETAPI", 'DEBUG', "API Return")
        self.logging['data'] = None

        # Handle Response
        if response.status_code == 200:
            # OK: handle json response and continue
            return response.json()

        if response.status_code == 401 or response.status_code == 404:

            # Generate refresh token and go again
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "REFTOKEN", 'DEBUG', "Refresh: Getting Token")

            # Call function to generate refresh token
            auth_code = self.auth.main_auth(inputs)

            if auth_code == "RETERROR":
                return auth_code
            # Rebuild headers with new auth code
            headers = self.function_class.get_headers(auth_code)

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "REFCALLAPI", 'DEBUG', "Refresh: Calling API")

            # Call API and handle response.
            try:
                response = call_api(
                    "get",
                    url,
                    headers,
                    params
                    )

                # If the response was successful, no Exception will be raised
                response.raise_for_status()

                internal.auth_logger(
                    self.exec_logger,
                    self.logging,
                    "REFRETAPI", 'DEBUG', "Refresh: API Return")

                r_json = response.json()

                return r_json

            except HTTPError as http_err:
                # Log out error and return out of function
                logging_val = {}
                logging_val['url'] = url
                logging_val['response_code'] = response.status_code
                logging_val['error'] = str(http_err)
                self.logging['data'] = json.dumps(logging_val)

                internal.auth_logger(
                    self.exec_logger,
                    self.logging,
                    "FAILHTTPERR", 'ERROR', 'HTTP error occurred')
                self.logging['data'] = None

                return "RETERROR"

        # If here: something went wrong. Log out error and return
        logging_val = {}
        logging_val['url'] = url
        logging_val['response_code'] = response.status_code
        self.logging['data'] = json.dumps(logging_val)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "CONFAIL", 'ERROR', 'Unable to Connect')
        self.logging['data'] = None

        return "RETERROR"

class ToolsetData():
    """
    Purpose:
    Toolset specific handling of data.

    Functions externally referenced:
    - get_events: central function to control data processing

    """
    def __init__(self, splunk_service, exec_logger, internal_functions, event):

        self.splunk_service = splunk_service
        self.exec_logger = exec_logger

        self.api = ToolsetAPI(self.splunk_service, self.exec_logger)
        self.function_class = ToolsetFunctions()
        #self.event_write = EventWriter()
        self.event_writer = internal.WriteEvent(event['builder'], event['writer'])
        self.internal_functions = internal_functions

        self.logging = {}
        self.logging['file'] = "toolset_main"
        self.logging['function_name'] = ""

    def enrich_logging(self, inputs):
        """
        Purpose:
        Enrich logging data

        INPUTS:
        - inputs: namedTuple built with values from inputs
        """
        self.logging['source'] = inputs.source
        self.logging['host'] = inputs.host
        self.logging['index'] = inputs.index

    def get_events(self, inputs, lastrun, currun):
        """
        Purpose:
        Reference required functions to collect data

        INPUTS:
        - inputs: namedTuple built with values from inputs
        - lastrun: datetime of last run. Used to prevent duplication of data
        - currun: current datetime. Future events will be collected in next run

        NOTE:
        For more complex inputs with optional event collection, this function should be used to
        process logic regarding what functions should be called. Separate functions should exist
        within this class to perform processing specific to each input
        """
        # Initialize Logging
        self.logging['function_name'] = "get_events"
        self.enrich_logging(inputs)

        logging_val = {}
        logging_val['lastrun'] = lastrun
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Get Toolset Event Activity
        cur_status = self.get_activity(inputs, lastrun, currun)
        self.logging['function_name'] = "get_events"

        if cur_status != "OK":
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "RETACTERROR", 'DEBUG', "Returning from Error in activity")
            return "ERROR"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "RETACT", 'DEBUG', "Getting Projects")

        return "OK"


    def get_activity(self, inputs, lastrun, currun):
        """
        Purpose:
        Get Event Activity from toolset and write resulting events

        INPUTS:
        - inputs: namedTuple built with values from inputs
        - lastrun: datetime of last run. Used to prevent duplication of data
        - currun: current datetime. Future events will be collected in next run

        """

        self.logging['function_name'] = "get_activity"
        self.enrich_logging(inputs)

        logging_val = {}
        logging_val['lastrun'] = lastrun
        logging_val['currun'] = currun
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")
        self.logging['data'] = None

        # Set sourcetype for event here
        sourcetype = "GitLab:Event"

        # Set parameters for API Call
        params = {"sort" : "desc"}

        # Call API
        data = self.api.get_data(inputs, "events", params=params)
        self.logging['function_name'] = "get_activity"

        # Error already handled in api class. Exit out if required
        if data == "RETERROR":
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "ERRHAND", 'DEBUG', "Error Handled")
            return "ERROR"

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "ITER", 'DEBUG', "Iterating through data")

        # Loop through returned dataset
        for event in data:

            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "LPST", 'DEBUG', "Loop Start")

            input_vars = {}
            input_vars['updated_at'] = event['created_at']
            input_vars['timestamp'] = event['created_at']
            input_vars['lastrun'] = lastrun
            input_vars['currun'] = currun
            input_vars['sourcetype'] = sourcetype

            cur_stat = self.get_events_in_array(inputs, event, input_vars)
            self.logging['function_name'] = "get_activity"

            if cur_stat == "FIN":
                break

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "LPOUT", 'DEBUG', "Loop Out")

        # Close write stream
        self.event_writer.close_event_write()

        return "OK"

    def get_events_in_array(self, inputs, event, input_vars):
        """
        Purpose:
        Process events in returned array and write resulting events

        INPUTS:
        - inputs: namedTuple built with values from inputs
        - event: json containing event to write
        - input_vars: dict used to contain all values used in function

        """
        self.logging['function_name'] = "get_events_in_array"
        self.enrich_logging(inputs)

        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "START", 'DEBUG', "Starting Function")

        # Check event has not already been ingested

        event_time = datetime.datetime.strptime(input_vars['updated_at'][:19], '%Y-%m-%dT%H:%M:%S')
        event_time = (event_time - datetime.datetime(1970, 1, 1)).total_seconds()
        event_time = float(event_time)
        lastrun = float(input_vars['lastrun']) if input_vars['lastrun'] != "Unknown" else "Unknown"
        currun = float(input_vars['currun'])

        if currun < lastrun:
            # Here if all new events since last run have been ingested
            logging_val = {}
            logging_val['currun'] = currun
            logging_val['event_created_at'] = str(event_time)
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "FUTEV", 'DEBUG', "Future Event")
            return "NEXT"

        if lastrun != "Unknown" and event_time < lastrun:
            # Here if all new events since last run have been ingested
            logging_val = {}
            logging_val['lastrun'] = lastrun
            logging_val['event_created_at'] = str(event_time)
            self.logging['data'] = json.dumps(logging_val)
            internal.auth_logger(
                self.exec_logger,
                self.logging,
                "EVFIN", 'DEBUG', "All Events Written")
            return "FIN"

        # Here if we need to write event
        logging_val = {}
        logging_val['lastrun'] = lastrun
        logging_val['event_created_at'] = str(event_time)
        self.logging['data'] = json.dumps(logging_val)
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "WREV", 'DEBUG', "Write Event")
        self.logging['data'] = None

        self.prep_event_write(inputs, event, input_vars['sourcetype'], input_vars['timestamp'])
        self.logging['function_name'] = "get_events_in_array"
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "FINFUNC", 'DEBUG', "Finishing Function")

        return "OK"

    def prep_event_write(self, inputs, event, sourcetype, timestamp):
        """
        Purpose:
        Get Event Activity from toolset and write resulting events

        INPUTS:
        - inputs: namedTuple built with values from inputs
        - event: event to write to index
        - sourcetype: event sourcetype
        - timestamp: event timestamp

        NOTE: USE THIS TO CALL WRITE EVENT FUNCTION
        """
        self.logging['function_name'] = "prep_event_write"
        # BLOCK MUST BE INCLUDED FOR TA MONITORING CONSOLE START -->
        self.logging['sourcetype'] = sourcetype
        internal.auth_logger(
            self.exec_logger,
            self.logging,
            "NEWEVENT", 'INFO', "Write Event")
        self.logging['sourcetype'] = None
        # <-- END TA MONITORING CONSOLE

        self.event_writer.write_out_event(inputs, event, sourcetype, timestamp)
